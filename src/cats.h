/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _CATS_H_
#define _CATS_H_

#include <lvgl.h>
#include <sys/dlist.h>

enum {
	CATS_SCREEN_HORIZONTAL,
	CATS_SCREEN_VERTICAL,
};

struct cats_screen {
	lv_obj_t *obj;
	const char *name;
	sys_dnode_t dnode;
	int orientation;
};

void cats_add_screen(struct cats_screen *scr);

#define CATS_SCREEN_INIT(func) SYS_INIT(func, APPLICATION, 99)

#endif /* _CATS_H_ */
